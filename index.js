// console.log("Hello World");
/*
    1. Declare 3 variables without initialization called username,password and role.
*/

let username;
let password;
let role;

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/

function loginUserInfo() {
    let loginUsername = prompt("Enter your username here:");
    let loginPassword = prompt("Enter your password here:");
    let loginRole = prompt("Enter your role here:") 
        if(loginUsername === "" || loginPassword === "" || loginRole === "" || loginUsername === "" || loginPassword === "" || loginRole === "") {
            alert("Input should not be empty!");
        } else {
            switch(loginRole) {
                case "admin":
                    console.log("Welcome back to the class portal, admin!");
                    break;
                case "teacher":
                    console.log("Thank you for logging in, teacher!");
                    break;
                case "student":
                    console.log("Welcome to the class portal, student!");
                    break;
                default:
                    console.log("Role out of range.");
                    break;
            }
        }
}

loginUserInfo();

/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/

function getAverage(grade1, grade2, grade3, grade4) {
    let grades = grade1 + "," + grade2 + ","+ grade3 + ","+ grade4 + ")";
    let aveComp = grade1 + grade2 + grade3 + grade4;
    let averageResult = Math.round(aveComp / 4);
    if(averageResult <= 74) {
        console.log("checkAverage(" + grades +  " \nHello, student, your average is " + averageResult + ". " + "The letter equivalent is F\n\n")
    } else if(averageResult >= 75 && averageResult <= 79) {
        console.log("checkAverage(" + grades +  " \nHello, student, your average is " + averageResult + ". " + "The letter equivalent is D\n\n")
    } else if(averageResult >= 80 && averageResult <= 84) {
        console.log("checkAverage(" + grades +  " \nHello, student, your average is " + averageResult + ". " + "The letter equivalent is C\n\n")
    } else if(averageResult >= 85 && averageResult <= 89) {
        console.log("checkAverage(" + grades +  " \nHello, student, your average is " + averageResult + ". " + "The letter equivalent is B\n\n")
    } else if(averageResult >= 90 && averageResult <= 95) {
        console.log("checkAverage(" + grades +  " \nHello, student, your average is " + averageResult + ". " + "The letter equivalent is A\n\n")
    } else if(averageResult >= 96) {
        console.log("checkAverage(" + grades +  " \nHello, student, your average is " + averageResult + ". " + "The letter equivalent is A+\n\n")
    }
}
getAverage(71, 70, 73, 74);
getAverage(75, 75, 76, 78);
getAverage(80, 81, 82, 78);
getAverage(84, 85, 87, 88);
getAverage(89, 90, 91, 90);
getAverage(91, 96, 97, 95);
getAverage(91, 96, 97, 99);


